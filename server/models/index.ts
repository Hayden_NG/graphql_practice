import Author from "./author";
import Book from "./book";

const models = {
  Author,
  Book,
};

export default models;
