import mongoose from "mongoose";
import { v4 } from "uuid";

const Schema = mongoose.Schema;

const authorSchema = new Schema({
  _id: { type: String, default: () => v4() },
  name: String,
  age: Number,
});

export default mongoose.models.Author ?? mongoose.model("Author", authorSchema);
