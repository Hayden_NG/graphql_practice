import mongoose from "mongoose";
import { v4 } from "uuid";

const Schema = mongoose.Schema;

const bookSchema = new Schema({
  _id: { type: String, default: () => v4() },
  name: String,
  genre: String,
  authorId: String,
});

export default mongoose.models.Book ?? mongoose.model("Book", bookSchema);
