import { graphqlHTTP } from "express-graphql";
import { schema } from "./schema/schema";
import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import cors from "cors";

const app = express();
dotenv.config();

mongoose.connect(process.env.MONGODB_URL);
mongoose.connection.once("open", () => {
  console.log("connected to datebase");
});

app.use(cors());

app.use(
  "/api/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(4000, () => {
  console.log("Listening on port 4000");
});
