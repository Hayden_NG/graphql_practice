import React, { useState } from "react";
import { graphql } from "react-apollo";
import { queries } from "queries";
import { BookDetail } from "components";

export const BookList = graphql(queries.getBooksQuery)((props: any) => {
  const [selectedBook, setSelectBook] = useState<string>();
  const isLoading = props?.data.loading;
  const displayBooks = () => {
    if (isLoading) {
      return "Loading Books...";
    } else {
      return props.data.books.map((book: any) => (
        <li key={book._id} onClick={() => setSelectBook(book._id)}>
          {book.name}
        </li>
      ));
    }
  };

  return (
    <div>
      <h3>Book List</h3>
      <ul id="book-list">{displayBooks()}</ul>
      {/* @ts-ignore */}
      <BookDetail bookId={selectedBook} />
    </div>
  );
});
