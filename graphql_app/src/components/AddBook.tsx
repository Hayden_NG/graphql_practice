import React, { useState } from "react";
import { graphql } from "react-apollo";
import { queries } from "queries";
import { flowRight as compose } from "lodash";
import { mutations } from "mutations";

export const AddBook = compose(
  graphql(mutations.addBookMutation, { name: "addBookMutation" }),
  graphql(queries.getAuthorsQuery, { name: "getAuthorsQuery" })
)((props: any) => {
  const [name, setName] = useState<string>();
  const [genre, setGenre] = useState<string>();
  const [authorId, setAuthorId] = useState<string>();

  const isLoading = props?.getAuthorsQuery.loading;
  const displayAuthors = () =>
    isLoading ? (
      <option disabled>Loading Author</option>
    ) : (
      props.getAuthorsQuery.authors.map((author: any) => (
        <option key={author._id} value={author._id}>
          {author.name}
        </option>
      ))
    );

  const newBookInfo = {
    name: name,
    genre: genre,
    authorId: authorId,
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    props.addBookMutation({
      variables: newBookInfo,
      refetchQueries: [{ query: queries.getBooksQuery }],
    });
  };

  return (
    <form id="add-book" onSubmit={(event) => handleSubmit(event)}>
      <div className="field">
        <label>Book name: </label>
        <input type="text" onChange={(e) => setName(e.target.value)} />
      </div>

      <div className="field">
        <label>Genre: </label>
        <input type="text" onChange={(e) => setGenre(e.target.value)} />
      </div>

      <div className="field">
        <label>Author: </label>
        <select onChange={(e) => setAuthorId(e.target.value)}>
          <option disabled selected hidden>
            Select Author
          </option>
          {displayAuthors()}
        </select>
      </div>
      <button type="submit">Add Book</button>
    </form>
  );
});
