import React from "react";
import { graphql } from "react-apollo";
import { queries } from "queries";

export const BookDetail = graphql(queries.getBookQuery, {
  options: (props: any) => {
    return {
      variables: {
        _id: props.bookId,
      },
    };
  },
})((props: any) => {
  const { book } = props?.data;

  const displayBookDetail = () => {
    if (book) {
      return (
        <div>
          <h2>{book.name}</h2>
          <p>Genre: {book.genre}</p>
          <p>Author: {book.author.name}</p>
          <p>All Books by this author: </p>
          <ul className="other-books">
            {book.author.books.map((book: any) => (
              <li key={book._id}>{book.name}</li>
            ))}
          </ul>
        </div>
      );
    } else {
      return <div>No book selected...</div>;
    }
  };
  return <div id="book-detail">{displayBookDetail()}</div>;
});
