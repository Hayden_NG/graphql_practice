import ApolloClient from "apollo-boost";
import React from "react";
import { ApolloProvider } from "react-apollo";
import { BookList, AddBook } from "components";
import dotenv from "dotenv";

dotenv.config();

const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL_URI,
});

export default function App() {
  return (
    <ApolloProvider client={client}>
      <div id="main">
        <h1>Reading List</h1>
        <BookList />
        <AddBook />
      </div>
    </ApolloProvider>
  );
}
