import { gql } from "apollo-boost";

export const getBooksQuery = gql`
  {
    books {
      name
      _id
    }
  }
`;

export const getAuthorsQuery = gql`
  {
    authors {
      name
      _id
    }
  }
`;

export const getBookQuery = gql`
  query($_id: String) {
    book(_id: $_id) {
      _id
      name
      genre
      author {
        _id
        name
        age
        books {
          name
          _id
        }
      }
    }
  }
`;
