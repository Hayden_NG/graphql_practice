import { getBooksQuery, getAuthorsQuery, getBookQuery } from "./queries";

export const queries = {
  getBooksQuery,
  getAuthorsQuery,
  getBookQuery,
};
